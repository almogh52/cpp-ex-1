#include <iostream>

#include "1\queue.h"
#include "2\linkedlist.h"
#include "2\stack.h"
#include "2/utils.h"

using namespace std;

int main()
{
    // Check array reverse
    cout << "Array reverse:" << endl;
    int nums[] = { 5, 9, 20, 15, 17, 16 };

    reverse(nums, 6);

    for (int i = 0; i < 6; i++)
    {
	cout << nums[i] << endl;
    }

    // Check array reverse 10
    cout << endl << "Array reverse 10:" << endl;
    int *arr = reverse10();

    for (int i = 0; i < 10; i++)
    {
	cout << arr[i] << endl;
    }

    // Check stack
    cout << endl << "Stack:" << endl;
    stack *s = new stack;

    initStack(s);

    push(s, 5);
    push(s, 7);

    cout << pop(s) << endl << endl;
    cout << pop(s) << endl << endl;
    cout << pop(s) << endl << endl;

    // Check linked list
    cout << "Linked list:" << endl;
    listNode *someNode = 0;

    insert(&someNode, 5);

    cout << "Head of list:" << endl;
    cout << someNode->value << endl;
    cout << someNode->next << endl << endl;

    insert(&someNode, 10);

    cout << "Head of list after addition of element:" << endl;
    cout << someNode->value << endl;
    cout << someNode->next << endl;

    cout << "The element after the head of the list:" << endl;
    cout << someNode->next->value << endl << endl;

    cout << "Removing all elements from the list:" << endl;
    cout << remove(&someNode) << endl << endl;
    cout << remove(&someNode) << endl << endl;
    cout << remove(&someNode) << endl << endl;

    // Check queue
    cout << "Queue:" << endl;
    queue *idk = new queue;

    initQueue(idk, 2);

    enqueue(idk, 5);
    enqueue(idk, 2);

    enqueue(idk, 3);

    cout << "Dequeuing elements from list:" << endl;
    std::cout << dequeue(idk) << std::endl << std::endl;
    std::cout << dequeue(idk) << std::endl << std::endl;
    std::cout << dequeue(idk) << std::endl << std::endl;

    enqueue(idk, 7);
    enqueue(idk, 9);

    std::cout << dequeue(idk) << std::endl << std::endl;

    enqueue(idk, 13);

    std::cout << dequeue(idk) << std::endl << std::endl;
    std::cout << dequeue(idk) << std::endl << std::endl;
    std::cout << dequeue(idk) << std::endl << std::endl;

    cleanQueue(idk);

    system("pause");
}