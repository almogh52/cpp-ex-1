#include <iostream>

#include "queue.h"

#define EMPTY_QUEUE -1

/*
* This function initializes an queue with a given max size.
* Input: The queue and it's max size.
* Output: The queue initialized with the max size given.
*/
void initQueue(queue* q, unsigned int size)
{
    // Initialize the queue array with the wanted size
    q->arr = new unsigned int[size];

    // Set the max size as the array size and reset the length of the queue
    q->maxSize = size;
    q->length = 0;

    // Set the current person waiting to be dequeued as the first element in the array
    q->front = 0;

    // Set the next available index in the queue as the first element
    q->rear = 0;
}

/*
* This function cleans a queue
* Input: The queue.
* Output: The queue cleaned and freed from the memory
*/
void cleanQueue(queue* q)
{
    // Delete the array of integers
    delete[] q->arr;

    // Delete the queue stuck pointer
    delete q;
}

/*
* This function enqueues an unsigned integer into a queue
* Input: The queue and the unsigned value to be enqueued.
* Output: The value enqueued in the list.
*/
void enqueue(queue* q, unsigned int newValue)
{
    // Check if we didn't reach the max size of the queue, if we reached it, ignore the enqueue
    if (q->length < q->maxSize)
    {
	// Insert the new value in the next available position which is the rear index
	q->arr[q->rear] = newValue;

	// Increase the length of the queue by 1
	q->length++;

	// Increase the rear index by 1, but keep it in the queue size range by modulo
	q->rear = (q->rear + 1) % q->maxSize;
    }
}

/*
* This function dequeues an unsigned integer from a queue
* Input: The queue.
* Output: The first element(integer) queued in the queue.
*/
int dequeue(queue* q)
{
    unsigned int frontValue = 0;

    // Check if the queue isn't empty, if it isn't return the element in the front index
    if (q->length > 0)
    {
	// Save the front value to be returned
	frontValue = q->arr[q->front];

	// Decrease the length of the list
	q->length--;
	
	// Increase the front index pointer to the next element in the queue, but keeping it in the queue size range by modulo
	q->front = (q->front + 1) % q->maxSize;

	return frontValue;
    }
    else { // If the queue is empty return -1
	return EMPTY_QUEUE;
    }
}