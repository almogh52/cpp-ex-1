#ifndef QUEUE_H
#define QUEUE_H

/* a queue contains positive integer values. */
typedef struct queue
{
    unsigned int *arr; // The array of the integers
    int maxSize; // The max size of the queue
    int length; // The current length of the queue
    int front; // The front integer's index waiting to be dequeued
    int rear; // The next available index in the queue to enqueued
} queue;

/*
* This function initializes an queue with a given max size.
* Input: The queue and it's max size.
* Output: The queue initialized with the max size given.
*/
void initQueue(queue* q, unsigned int size);

/*
* This function cleans a queue
* Input: The queue.
* Output: The queue cleaned and freed from the memory
*/
void cleanQueue(queue* q);

/*
* This function enqueues an unsigned integer into a queue
* Input: The queue and the unsigned value to be enqueued.
* Output: The value enqueued in the list.
*/
void enqueue(queue* q, unsigned int newValue);

/*
* This function dequeues an unsigned integer from a queue
* Input: The queue.
* Output: The first element(integer) queued in the queue.
*/
int dequeue(queue* q);

#endif /* QUEUE_H */