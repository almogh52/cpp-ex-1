#include <iostream>
#include "stack.h"

using namespace std;

#define ARR_SIZE 10

/*
* This function reverses an array of integers.
* Input: The pointer to the start of the array and the size of the array.
* Output: The array reversed.
*/
void reverse(int* nums, unsigned int size)
{
    stack *stk = new stack;

    // Initialize the stack
    initStack(stk);

    // Push all the elements from the array into the stack in order to flip them
    for (unsigned int i = 0; i < size; i++)
    {
	// Push the element into the stack
	push(stk, nums[i]);
    }

    // Pop all the elements from the stack in the oppsite order
    for (unsigned int i = 0; i < size; i++)
    {
	// Pop a number from the stack and set it in the array
	nums[i] = pop(stk);
    }
}

/*
* This function gets from the user 10 integers into an array and reverses it.
* Input: 10 integers from the user.
* Output: The array of the 10 integers flipped.
*/
int* reverse10()
{
    // Create a dynamic array with 10 values
    int *arr = new int[ARR_SIZE];

    // Get the 10 numbers to the array
    for (int i = 0; i < ARR_SIZE; i++)
    {
	// Ask for input from the user
	cout << "Please enter a number " << i + 1 << ": ";

	// Get a number from the user
	cin >> arr[i];
    }

    // Reverse the array
    reverse(arr, ARR_SIZE);

    return arr;
}