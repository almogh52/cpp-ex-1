#include "linkedlist.h"

/*
* This function inserts a value into the start of a linked list.
* Input: The unsigned value and the pointer to the head of the list (which is a pointer as well)
* Output: The value inserted in the start of the list.
*/
void insert(listNode **head, unsigned int value)
{
    listNode *newNode = new listNode();

    // Set the value in the new node
    newNode->value = value;

    // If the head of the list is empty(the pointer is null), set the head of the list as the new node pointer
    if (!(*head))
    {
	// Create the new list noe
	*head = newNode;

	// Set the next value in the list as null
	newNode->next = 0;
    }
    else {
	// Set the next node of the new node as the previous head of the list
	newNode->next = *head;

	// Set the new node as the head of the list
	*head = newNode;
    }
}

/*
* This function removes the first element in a linked list.
* Input: The pointer to the head of the list (which is a pointer as well)
* Output: The value in the head of the list.
*/
unsigned int remove(listNode **head)
{
    listNode *headPtr = 0;
    unsigned int value = 0;

    // If the head of the list isn't empty
    if (*head)
    {
	// Save the head node and the value of the head
	headPtr = *head;
	value = (*head)->value;
	
	// Set the head of the list as the next element after the head
	*head = headPtr->next;

	// Destory the head element
	delete headPtr;
    }

    return value;
}