#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/* a linked list struct */
typedef struct listNode
{
    unsigned int value; // The value the node is holding
    listNode *next; // The pointer to the next element in the list
} listNode;

/*
* This function inserts a value into the start of a linked list.
* Input: The unsigned value and the pointer to the head of the list (which is a pointer as well)
* Output: The value inserted in the start of the list.
*/
void insert(listNode **head, unsigned int value);

/*
* This function removes the first element in a linked list.
* Input: The pointer to the head of the list (which is a pointer as well)
* Output: The value in the head of the list.
*/
unsigned int remove(listNode **head);

#endif // LINKEDLIST_H