#ifndef STACK_H
#define STACK_H

#include "linkedlist.h"

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
    listNode *listHead; // The pointer to the head of the list
} stack;

/*
* This function pushes an element into a stack
* Input: The stack and the element to be pushed (unsigned integer).
* Output: The integer pushed into the stack.
*/
void push(stack* s, unsigned int element);

/*
* This function pops an element from a stack
* Input: The stack.
* Output: The last element pushed into the stack or -1 if empty.
*/
int pop(stack* s);

/*
* This function initializes a stack
* Input: The stack.
* Output: The stack initialized.
*/
void initStack(stack* s);

/*
* This function cleans a stack.
* Input: The stack.
* Output: The stack cleaned and freed from the memory
*/
void cleanStack(stack* s);

#endif // STACK_H