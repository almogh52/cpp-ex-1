#include "stack.h"
#include "linkedlist.h"

#define EMPTY_STACK -1

/*
* This function initializes a stack
* Input: The stack.
* Output: The stack initialized.
*/
void initStack(stack* s)
{
    // Set the head of the list as null since the list is empty
    s->listHead = 0;
}

/*
* This function pushes an element into a stack
* Input: The stack and the element to be pushed (unsigned integer).
* Output: The integer pushed into the stack.
*/
void push(stack* s, unsigned int element)
{
    // Insert the new element to the list of elements
    insert(&(s->listHead), element);
}

/*
* This function pops an element from a stack
* Input: The stack.
* Output: The last element pushed into the stack or -1 if empty.
*/
int pop(stack* s)
{
    int firstValue = EMPTY_STACK;

    // If the list isn't empty, remove an element from it
    if (s->listHead)
    {
	// Remove an element from the list
	firstValue = remove(&(s->listHead));
    }

    return firstValue;
}

/*
* This function cleans a stack.
* Input: The stack.
* Output: The stack cleaned and freed from the memory
*/
void cleanStack(stack* s)
{
    // While the list isn't empty, keep popping elements out of it
    while (s->listHead)
    {
	remove(&(s->listHead));
    }

    // Delete the stack struct
    delete s;
}